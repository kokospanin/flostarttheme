<?php
global $post;
wp_redirect($post->post_parent ? get_permalink($post->post_parent) : site_url());
exit;