<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$context['contact_page'] = flo_get_page('contact');
$context['contact_page']->right_side_info = get_field('right_side_info', $context['contact_page']->ID);

Timber::render('pages/404.twig', $context);