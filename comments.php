<?php
	// Do not delete these lines for security reasons
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) {
		die ('Please do not load this page directly. Thanks!');
	}

	$context = Timber::get_context();
	$context['user'] = wp_get_current_user();
	$context['post'] = Timber::query_post();

	Timber::render('views/partials/comments.twig', $context);
?>