<?php
/*********************************************************************
 * DO NOT DELETE
 ********************************************************************/

if ( STYLESHEETPATH == TEMPLATEPATH ) {
	define('FLOTHEME_PATH', TEMPLATEPATH . '/flolib');
	define('FLOTHEME_URL', get_bloginfo('template_directory') . '/flolib');
} else {
	
	define('FLOTHEME_PATH', STYLESHEETPATH . '/flolib');
	define('FLOTHEME_URL', get_bloginfo('stylesheet_directory') . '/flolib');
}

require_once FLOTHEME_PATH . '/init.php';
require_once FLOTHEME_PATH . '/timber-library/timber.php';

/*********************************************************************
 * Please add your custom functions in 'flolib/functions/custom.php'
 ********************************************************************/