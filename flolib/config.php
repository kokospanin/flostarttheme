<?php


/**
 * Add post types that are used in the theme
 * Example: http://projects.flosites.com/projects/flosites-coretheme/wiki/Post_Types_Example
 * @return array
 */
function flotheme_get_post_types() {
    return array(
        'work' => array(
            'config' => array(
                'public' => true,
                'exclude_from_search' => true,
                'menu_position' => 20,
                'has_archive'   => false,
                'supports'=> array(
                    'title',
                    'editor',
                    'page-attributes',
                    'thumbnail',
                ),
                'show_in_nav_menus'=> true,
                'rewrite' => array(
                    'slug' => 'work',
                ),
            ),
            'singular' => 'Work',
            'multiple' => 'Works',
            'columns'       => array(
                'featured',
            ),
            'per_page' => -1,
        ),

        'team' => array(
            'config' => array(
                'public' => true,
                'exclude_from_search' => true,
                'menu_position' => 20,
                'has_archive'   => true,
                'supports'=> array(
                    'title',
                    'page-attributes',
                    'thumbnail',
                    'editor'
                ),
                'show_in_nav_menus'=> true,
            ),
            'singular' => 'Employee',
            'multiple' => 'Team',
            'columns'       => array(
                'featured',
            ),
            'per_page' => -1,
        ),

    );
}

/**
 * Add taxonomies that are used in theme
 * Example: http://projects.flosites.com/projects/flosites-coretheme/wiki/Taxonomy_Example
 * @return array
 */
function flotheme_get_taxonomies() {
	return array(
        // 'gallery-category'    => array(
        //     'for'        => array('gallery'),
        //     'config'    => array(
        //         'sort'        => true,
        //         'args'        => array('orderby' => 'term_order'),
        //         'hierarchical' => true,
        //     ),
        //     'singular'    => 'Category',
        //     'multiple'    => 'Categories',
        // ),
	);
}

/**
 * Get image sizes for images
 * Example: http://projects.flosites.com/projects/flosites-coretheme/wiki/Images_Sizes_Example
 * @return array
 */
function flotheme_get_images_sizes() {
        return array(
                'team' => array(
                    array(
                        'name'     => 'team-portrait',
                        'width'    => 720,
                        'height'   => 840,
                        'crop'     => false,
                    ),                        
                    array(
                        'name'     => 'team-thumb',
                        'width'    => 480,
                        'height'   => 480,
                        'crop'     => true,
                    ),
                ),
        );
}

/**
 * Get responsive breakpoints
 * @return array
 */

function flotheme_get_breakpoints() {
        return array(480,640,1024,1920);
}

/**
 * Get responsive image sizes for images
 * sizes  will be automatically generated for each of responsive breakpoint
 * @return array
 */
function flotheme_get_responsive_images_sizes() {
        return array(
                // 'gallery' => array(
                //         'name'   => 'gallery-cover',
                //         'width'  => 1920,
                //         'height' => 754,
                //         'crop'   => true,
                // ),
        );
}

/**
 * Add post formats that are used in theme
 *
 * @return array
 */
function flotheme_get_post_formats() {
	return array(
		// uncomment if you want to use post formats
		// 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'
	);
}

/**
 * Get sidebars list
 *
 * @return array
 */
function flotheme_get_sidebars() {
	$sidebars = array(
		// uncomment if you want to use sidebars
		// you can add new sidebar ex.: 'id' => 'title'
		// you can display specific sidebar with function dynamic_sidebar('id')
		// 'general-sidebar' => 'General Sidebar'
	);
	return $sidebars;
}

/**
 * Post types where metaboxes should show
 * @return array
 */
function flotheme_get_post_types_with_gallery() {
	return array('post');
}
