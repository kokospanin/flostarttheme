<?php


/**
 * Display share options
 * 
 * @param string $type 
 */
function flo_share($type = 'fb') {
    echo flo_get_share($type);
}

/**
 * Get link/code for sharing
 * @param  string  $type      Social network key
 * @param  string $permalink  Page URL
 * @param  string $title      Page title
 * @param  string $media      Image URL
 * @return string             Link or button code
 */
function flo_get_share($type = 'fb', $permalink = false, $title = false, $media = false) {
    if (!$permalink) {
        $permalink = urlencode(get_permalink());
    }
    if (!$title) {
        $title = get_the_title();
    }
    if(!$media) {
        $attachment = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ) , 'full', false );
        $media = is_array($attachment) ? urlencode($attachment[0]) : "";
    }

    switch ($type) {
        case 'twi': // Twitter
            return 'http://twitter.com/home?status=' . $title . '+-+' . $permalink;
            break;            
        case 'tweet': // Twitter button
            return '<a href="http://twitter.com/share" class="twitter-share-button" data-url="' . $permalink . '" data-count="horizontal">Tweet</a>';
            break;
        case 'fb': // Facebook
            return 'http://www.facebook.com/sharer.php?u=' . $permalink . '&t=' . $title;
            break;
        case 'like': // Facebook like button
            return '<iframe src="http://www.facebook.com/plugins/like.php?href=' . urlencode($permalink) . '&amp;send=false&amp;layout=button_count&amp;width=80&amp;show_faces=false&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:21px;" allowTransparency="true"></iframe>';
            break;
        case 'gplus': // Google+
            return 'https://plus.google.com/share?url='. $permalink . '&title=' . $title;
            break;
        case 'plus1': // Google+ button
            return '<g:plusone size="medium" href="' . $permalink . '"></g:plusone>';
            break;
        case 'pin': // Pinterest
            return 'http://pinterest.com/pin/create/button/?url=' . $permalink . '&media=' . $media . '&title=' . $title;
            break;
        default:
            return '';
    }
}
