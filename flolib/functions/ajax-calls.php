<?php

/**
 * Add combined actions for AJAX.
 *
 * @param string $tag
 * @param string $function_to_add
 * @param integer $priority
 * @param integer $accepted_args
 */
function flo_add_ajax_action($tag, $function_to_add, $priority = 10, $accepted_args = 1) {
	add_action('wp_ajax_' . $tag, $function_to_add, $priority, $accepted_args);
	add_action('wp_ajax_nopriv_' . $tag, $function_to_add, $priority, $accepted_args);
}


/**
 * Comment callback function
 * @param object $comment
 * @param array $args
 * @param int $depth
 */
function flotheme_comment($comment, $args, $depth) {
	Timber::render('views/partials/comment.twig', $comment);
	$GLOBALS['comment'] = $comment; 
}


/**
 * AJAXify comments
 * @global object $user
 * @param int $comment_ID
 * @param int $comment_status
 */
function flo_post_comment_ajax($comment_ID, $comment_status) {
	global $user;
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$comment = get_comment($comment_ID);

        switch($comment_status){
            case '0':
                //notify moderator of unapproved comment
                wp_notify_moderator($comment_ID);
            case '1': //Approved comment
                $post=&get_post($comment->comment_post_ID); //Notify post author of comment
                if ( get_option('comments_notify') && $comment->comment_approved && $post->post_author != $comment->user_id )
                    wp_notify_postauthor($comment_ID, $comment->comment_type);
                break;
            default:
                echo json_encode(array(
					'error' => 1,
					'msg'	=> __('Something went wrong. Please refresh page and try again.', 'flotheme'),
				));exit;
        }
		// save cookie for non-logged user.
		if ( !$user->ID ) {
			$comment_cookie_lifetime = apply_filters('comment_cookie_lifetime', 30000000);
			setcookie('comment_author_' . COOKIEHASH, $comment->comment_author, time() + $comment_cookie_lifetime, COOKIEPATH, COOKIE_DOMAIN);
			setcookie('comment_author_email_' . COOKIEHASH, $comment->comment_author_email, time() + $comment_cookie_lifetime, COOKIEPATH, COOKIE_DOMAIN);
			setcookie('comment_author_url_' . COOKIEHASH, esc_url($comment->comment_author_url), time() + $comment_cookie_lifetime, COOKIEPATH, COOKIE_DOMAIN);
		}

		// load a comment to variable
		ob_start();
		flotheme_comment($comment, array('max_depth' => 1), 1);
		$html = ob_get_clean();

		echo json_encode(array(
			'html'		=> $html,
			'success'	=> 1,
		));
		exit;
    }
}
if( !is_admin() ) {
	add_action('comment_post', 'flo_post_comment_ajax', 20, 2);
}


/**
* Load Team Member Profile AJAX Hook
*/
function flo_load_post()
{

    $post_id = (int) $_POST['post_id'];
	
	$context = Timber::get_context();

    if (!empty($post_id)) {
		$post = get_post($post_id);
		$context['post'] = $post;		

    	$thumbnail_id = get_post_thumbnail_id( $post_id );
    	$context['post']->thumbnail = wp_get_attachment_url($thumbnail_id, 'large');

		Timber::render('views/partials/post_profile.twig', $context);
	}

    exit;
}

add_action('wp_ajax_flotheme_load_post', 'flo_load_post');
add_action('wp_ajax_nopriv_flotheme_load_post', 'flo_load_post');

