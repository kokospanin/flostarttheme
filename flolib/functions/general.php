<?php

/**
 * Add data to Timber context
 *
 * @param array| $data
 * @param array| $data
 */

function flo_add_to_context($data){ 

    $data['options'] = get_fields('options');

    $data['menu'] = new TimberMenu(2); 


    $data['search_query'] = get_search_query(); 

    $data['is_single'] = is_single();

    $data['is_front'] = is_front_page();

    $data['blog_cats'] = get_categories(array(
        'hierarchical' => 0, 
        'hide_empty' => 0
    ));     

    if(function_exists('wpmd_is_phone')) {
        $data['is_phone'] = wpmd_is_phone();
    }
    
    return $data;
}
add_filter('timber_context', 'flo_add_to_context');


/**
 * Display permalink
 *
 * @param int|string $system
 * @param int $isCat
 */
function flo_permalink($system, $isCat = false) {
    echo flo_get_permalink($system, $isCat);
}
/**
 * Get permalink for page, post or category
 *
 * @param int|string $system
 * @param bool $isCat
 * @return string
 */
function flo_get_permalink($system, $isCat = 0)  {
    if ($isCat) {
        if (!is_numeric($system)) {
            $system = get_cat_ID($system);
        }
        return get_category_link($system);
    } else {
        $page = flo_get_page($system);

        return null === $page ? '' : get_permalink($page->ID);
    }
}

/**
 * Display custom excerpt
 */
function flo_excerpt() {
    echo flo_get_excerpt();
}
/**
 * Get only excerpt, without content.
 *
 * @global object $post
 * @return string
 */
function flo_get_excerpt() {
    global $post;
	$excerpt = trim($post->post_excerpt);
	$excerpt = $excerpt ? apply_filters('the_content', $excerpt) : '';
    return $excerpt;
}

/**
 * Display first category link
 */
function flo_first_category() {
    $cat = flo_get_first_category();
	if (!$cat) {
		echo '';
		return;
	}
    echo '<a href="' . flo_get_permalink($cat->cat_ID, true) . '">' . $cat->name . '</a>';
}
/**
 * Parse first post category
 */
function flo_get_first_category() {
    $cats = get_the_category();
    return isset($cats[0]) ? $cats[0] : null;
}

/**
 * Get page by name, id or slug.
 * @global object $wpdb
 * @param mixed $name
 * @return object
 */
function flo_get_page($slug) {
    global $wpdb;

    if (is_numeric($slug)) {
        $page = get_page($slug);
    } else {
        $page = $wpdb->get_row($wpdb->prepare("SELECT DISTINCT * FROM $wpdb->posts WHERE post_name=%s AND post_status=%s", $slug, 'publish'));
    }

    return $page;
}

/**
 * Find all subpages for page
 * @param int $id
 * @return array
 */
function flo_get_subpages($id) {
    $query = new WP_Query(array(
        'post_type'         => 'page',
        'orderby'           => 'menu_order',
        'order'             => 'ASC',
        'posts_per_page'    => -1,
        'post_parent'       => (int) $id,
    ));

    $entries = array();
    while ($query->have_posts()) : $query->the_post();
        $entry = array(
            'id' => get_the_ID(),
            'title' => get_the_title(),
            'link' => get_permalink(),
            'content' => get_the_content(),
        );
        $entries[] = $entry;
    endwhile;
    wp_reset_query();
    return $entries;
}

function flo_page_links() {
	global $wp_query, $wp_rewrite;
	$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

	$pagination = array(
		'base' => @add_query_arg('page','%#%'),
		'format' => '',
		'total' => $wp_query->max_num_pages,
		'current' => $current,
		'show_all' => false,
		'type' => 'list',
		'next_text' => 'Next page',
		'prev_text' => 'Prev. page'
		);

	if( $wp_rewrite->using_permalinks() )
		$pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

	if( !empty($wp_query->query_vars['s']) )
		$pagination['add_args'] = array( 's' => str_replace( " ", '+', get_query_var( 's' ) ) );

	echo paginate_links($pagination);
}


/**
 * Generate random number
 *
 * Creates a 4 digit random number for used
 * mostly for unique ID creation.
 *
 * @return integer
 */
function flo_get_random_number() {
	return substr( md5( uniqid( rand(), true) ), 0, 4 );
}

/**
 * Display custom RSS url
 */
function flo_rss() {
    echo flo_get_rss();
}

/**
 * Get custom RSS url
 */
function flo_get_rss() {
    return get_bloginfo('rss2_url');
}

/**
 * Get template part
 *
 * @param string $slug
 * @param string $name
 */
function flo_part($slug, $name = null) {
	get_template_part('partials/' . $slug, $name);
}

function flo_page_part($slug, $folder, $name = null) {
    get_template_part('partials/' . $folder . '/' . $slug, $name);
}

/**
 * Page Title Wrapper
 * @param type $title
 */
function flo_page_title($title) {
	echo flo_get_page_title($title);
}
function flo_get_page_title($title) {
	return '<header class="page-title"><h2>' . __($title, 'flotheme') . '</h2></header>';
}

/**
 * Get archives by year
 *
 * @global object $wpdb
 * @param string $year
 * @return array
 */
function flo_archives_get_by_year($year = "") {
	global $wpdb;

	$where = "";
	if (!empty($year)) {
		$where = "AND YEAR(post_date) = " . ((int) $year);
	}
	$query = "SELECT DISTINCT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, DATE_FORMAT(post_date, '%b') AS `abmonth`, DATE_FORMAT(post_date, '%M') AS `fmonth`, count(ID) as posts
									FROM $wpdb->posts
							WHERE post_type = 'post' AND post_status = 'publish' $where
									GROUP BY YEAR(post_date), MONTH(post_date)
									ORDER BY post_date DESC";

	return $wpdb->get_results($query);
}

/**
 * Get archives years list
 *
 * @global object $wpdb
 * @return array
 */
function flo_archives_get_years() {
	global $wpdb;

	$query = "SELECT DISTINCT YEAR(post_date) AS `year`
									FROM $wpdb->posts
							WHERE post_type = 'post' AND post_status = 'publish'
									GROUP BY YEAR(post_date) ORDER BY post_date DESC";

	return $wpdb->get_results($query);
}

/**
 * Get archives months list
 *
 * @return type
 */
function flo_archives_get_months() {
	return array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
}

/**
 * Display Archives
 */
function flo_archives($delim = '&nbsp;/&nbsp;') {
    $year = null;
    ?>
    <div class="flo-archives cf">
		<h4 class="a"><?php _e('Archives', 'flotheme');?></h4>
        <?php
            $months = flo_archives_get_months();
            $archives = flo_archives_get_by_year();
        ?>
        <div class="year">
            <span id="archives-active-year"></span>
            <a href="#" class="up">&gt;</a>
            <a href="#" class="down">&lt;</a>
        </div>
        <div class="months">
            <?php foreach ($archives as $archive) : ?>
                <?php
                    if ($year == $archive->year) {
                        continue;
                    }
                    $year = $archive->year;
                    $y_archives = flo_archives_get_by_year($archive->year);
                ?>
                <div class="year-months" id="archive-year-<?php echo $year?>">
                <?php foreach ($months as $key => $month) :?>
                    <?php foreach ($y_archives as $y_archive) :?>
                        <?php if (($key == ($y_archive->month-1)) && $y_archive->posts):?>
                            <a href="<?php echo get_month_link($year, $y_archive->month)?>"><?php _e($month, 'flotheme') ?></a>
                            <?php if ($key != 11 && $delim):?>
                                <span class="delim"><?php echo $delim; ?></span>
                            <?php endif;?>
                            <?php break;?>
                        <?php endif;?>
                    <?php endforeach;?>
                    <?php if ($key != $y_archive->month-1):?>
                        <span><?php _e($month, 'flotheme') ?></span>
                        <?php if ($key != 11 && $delim):?>
							<span class="delim"><?php echo $delim; ?></span>
                        <?php endif;?>
                    <?php endif;?>
                <?php endforeach;?>
                </div>
            <?php endforeach;?>
        </div>
    </div>
<?php
}


/**
 * Check if it's a blog page
 * @global object $post
 * @return boolean
 */
function flo_is_blog () {
	global  $post;
	$posttype = get_post_type($post);
	return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ($posttype == 'post')) ? true : false ;
}

function flo_get_blog_title() {
    $blog_title = '';

    if (is_category()) {
        $blog_title = 'Category: ' . single_cat_title( '', false );
    } else if (is_tag()) {
        $blog_title = 'Tag: ' . single_tag_title( '', false );
    } else if (is_author()) {
        if (have_posts()) {
            the_post();
            $blog_title = 'Author: ' . get_the_author();
        }
        rewind_posts();
    } else if (is_search()) {
        $blog_title = sprintf( __( 'Search Results for: %s', 'flotheme' ), '<span>' . get_search_query() . '</span>' );
    } else if (is_archive()) {
        if (is_day()) {
            $blog_title = 'Daily Archives: ' . get_the_date();
        } elseif (is_month()) {
            $blog_title = 'Monthly Archives ' . get_the_date( _x( 'F Y', 'monthly archives date format', 'flotheme'));
        } elseif (is_year()) {
            $blog_title = 'Yearly Archives ' . get_the_date( _x( 'Y', 'yearly archives date format', 'flotheme'));
        } else {
            $blog_title = 'Blog';
        }
    } else {
        $blog_title = '';
    }

    return $blog_title;
}

function flo_get_respond_form() {
    $context = Timber::get_context();
    $context['user'] = wp_get_current_user();
    $context['post'] = Timber::query_post();

    Timber::render('views/partials/respond.twig', $context);

}