<?php

/**
 * Remove HTML attributes from comments
 */
add_filter( 'comment_text', 'wp_filter_nohtml_kses' );
add_filter( 'comment_text_rss', 'wp_filter_nohtml_kses' );
add_filter( 'comment_excerpt', 'wp_filter_nohtml_kses' );

/**
 * Remove junk from head
 */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'feed_links_extra', 3 );
remove_action('wp_head', 'print_emoji_detection_script', 7 );
remove_action('wp_print_styles', 'print_emoji_styles' );

/**
 * This filter adds query for post search only.
 *
 * @param object $query
 * @return object
 */
function flo_exclude_search_pages($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}

	return $query;
}
if( !is_admin() ) add_filter('pre_get_posts', 'flo_exclude_search_pages');

/**
 * Load needed options & translations into template.
 */
function flo_init_js_vars() {
	wp_localize_script(
		'flo_scripts',
		'flo',
		array(
			'template_dir'      => THEME_URL,
			'ajax_load_url'     => site_url('/wp-admin/admin-ajax.php'),
			'is_mobile'         => (int) wp_is_mobile(),
		)
	);
}
add_action('wp_print_scripts', 'flo_init_js_vars');

/**
 * Enqueue Theme Styles
 */
function flo_enqueue_styles() {

	// add general css file
	wp_register_style( 'flotheme_general_css', THEME_URL . '/css/general.css', array(), null, 'all');
	wp_enqueue_style('flotheme_general_css');
}
add_action( 'wp_enqueue_scripts', 'flo_enqueue_styles' );

/**
 * Enqueue Theme Scripts
 */
function flo_enqueue_scripts() {

	// load jquery from google cdn
	// wp_deregister_script('jquery');
	wp_register_script( 'flo_scripts', THEME_URL . '/js/min/scripts.min.js', array(), null, true );	
	wp_enqueue_script( 'flo_scripts' );
}
add_action( 'wp_enqueue_scripts', 'flo_enqueue_scripts');

/**
 * Add header information
 */
function flo_head() {
	?>
			
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php flo_rss(); ?>" />
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<?php
}
add_action('wp_head', 'flo_head');


/**
 * Add footer information
 * Social Services Init
 */
function flo_footer() {
	?>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script type="text/javascript" src="http://assets.pinterest.com/js/pinit.js"></script>
	<?php
}

add_action('wp_footer', 'flo_footer');

