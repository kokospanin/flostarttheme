'use strict';

var gulp = require('gulp'),
	gutil = require('gulp-util'),
	notify = require('gulp-notify'),
	sass = require('gulp-sass'),
	plumber = require('gulp-plumber'),
	browserSync = require('browser-sync'),
	autoprefixer = require('gulp-autoprefixer'),
	uglify = require('gulp-uglify'),
	jshint = require('gulp-jshint'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	minifyCSS = require('gulp-minify-css');

 

gulp.task('sass', function(){
	return gulp.src('css/general.sass')
		.pipe(sass())	
   		.pipe(autoprefixer(                             
            'last 2 version',
            '> 1%',
            'ie 8',
            'ie 9',
            'ios 6',
            'android 4'
        ))			
		.pipe(minifyCSS())
		.pipe(gulp.dest('css/'))
		.pipe(browserSync.reload({stream:true}))
		.pipe(notify({message: 'SASS processed!'}));
});

gulp.task('fontstyles', function(){
	return gulp.src('css/fonts.sass')
		.pipe(sass())		
		.pipe(minifyCSS())
		.pipe(gulp.dest('css/'));
});
 

 gulp.task('js', function () {
 	return gulp.src('js/*.js')    
        .pipe(concat('scripts.js'))		
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('js/min/'))
		.pipe(browserSync.reload({stream:true, once: true}))
		.pipe(notify({message: 'JS processed!'}));
});
 
  
gulp.task('browser-sync', function() {
    browserSync({
        proxy: 'localhost/flo2015',
        // ghostMode: false,
        // tunnel: true,
        // online: false,
        // logConnections: true
    });
});
 
gulp.task('bs-reload', function () {
    browserSync.reload;
});
 
 
// Default Task
gulp.task('default', ['fontstyles', 'sass', 'js', 'browser-sync'], function () {
	gulp.watch('css/fonts.sass', ['fontstyles']);
    gulp.watch('css/*.sass', ['sass', 'bs-reload']);    
    gulp.watch('js/*.js', ['js']);
    gulp.watch('./**/*.php', ['bs-reload']);
    gulp.watch('./**/*.twig', ['bs-reload']);
});

