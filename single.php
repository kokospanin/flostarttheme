<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

global $post;
$context = Timber::get_context();
$context['post'] = $post;
Timber::render('single.twig', $context);
