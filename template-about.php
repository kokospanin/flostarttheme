<?php 
/**
 * Template Name: Template About
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render(array('templates/about.twig'), $context);