<?php 
/**
 * Template Name: Template Home
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render(array('templates/home.twig'), $context);